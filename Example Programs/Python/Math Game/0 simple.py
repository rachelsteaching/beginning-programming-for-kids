# Add random number support
import random

# print = output text to the screen
print( "MATH GAME" )

# Variables number1 and number2
# being assigned to random numbers between 1 and 10
number1 = random.randint( 1, 10 )
number2 = random.randint( 1, 10 )

print( "What is" )
print( number1 )
print( "plus" )
print( number2 )
print( "" )

# Get integer (whole numbers) as input from the player
answer = int( input( "Answer: " ) )

if ( answer == number1 + number2 ):     # Their answer was right
    print( "Correct!" ) 
else:                                   # Their answer was wrong
    print( "Wrong!" )
