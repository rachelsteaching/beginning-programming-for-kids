
# Ask the user to enter some text
adjective1  = raw_input( "Enter an adjective: " )
verb1       = raw_input( "Enter a verb (to ____): " )
verb2       = raw_input( "Enter a verb (to ____): " )
job1        = raw_input( "Enter a job: " )
location1   = raw_input( "Enter a location: " )
emotion     = raw_input( "Enter a negative emotion: " )
name1       = raw_input( "Enter a name: " )
name2       = raw_input( "Enter another name: " )







# Tell a story
print( "" )
print( "" )
print( name1 + " and " + name2 )
print( "By YOU!" )
print( "" )
print( "Once there was a " + adjective1 + " " + job1 + " named " + name1 )
print( name1 + " liked to " + verb1 + " but couldn't anymore because of " + name2 )
print( name2 + " hated to " + verb1 + " and insisted that " + name1 + " shouldn't." )
print( "" )
print( "One day, " + name1 + " went on a vacation to " + location1 + " without " + name2 )
print( name1 + " tried to " + verb1 + ", but they had forgotten how." )
print( name1 + " decided to " + verb2 + " instead, and got really good at it." )
print( "" )
print( "When " + name1 + " returned home, they showed " + name2 + " their " + verb2 + " skill." )
print( name2 + " didn't like that, either, and told " + name1 + " to stop doing it." )
print( name1 + " was " + emotion + " and decided that, whether " + name2 + " liked it or not," )
print( "they would continue to " + verb1 + " and " + verb2 + " on their own." )
print( "" )
print( "The End" )
